<?php

namespace Planet17\CLIProcesses\Processes;

/**
 * Class Process.
 *
 * @package Planet17\CLIProcesses\Processes
 */
class Process extends BaseProcess
{
    /** @inheritdoc  */
    public function getCommand(): string
    {
        return $this->command;
    }
}
