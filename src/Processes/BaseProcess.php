<?php

namespace Planet17\CLIProcesses\Processes;

use Planet17\CLIProcesses\Exceptions\HasBeenExecutedException;
use Planet17\CLIProcesses\Interfaces\ProcessInterface;

/**
 * Class BaseProcess
 *
 * @package Planet17\MessageQueueProcessManager\Processes
 */
abstract class BaseProcess implements ProcessInterface
{
    /** @var string $command */
    protected $command;

    /** @var bool $hasExecuted */
    protected $hasExecuted = false;

    /** @var mixed $output */
    private $output;

    /** @inheritdoc  */
    abstract public function getCommand(): string;

    /** @inheritdoc */
    public function __construct(string $command)
    {
        $this->command = $command;
    }

    /** @inheritdoc */
    final public function execute(): ProcessInterface
    {
        $this->ensureNotExecutedBefore();
        $this->executeCommand();

        return $this;
    }

    /**
     * Method just check one time execution for process.
     */
    protected function ensureNotExecutedBefore(): void
    {
        if ($this->hasExecuted) {
            throw new HasBeenExecutedException;
        }
    }

    /**
     * Method execute command and mark
     */
    final protected function executeCommand(): void
    {
        $this->output = exec($this->getCommand()) . PHP_EOL;
        $this->hasExecuted = true;
    }

    /**
     * Method return result of cli executed.
     *
     * @return mixed
     */
    final public function getOutput()
    {
        return $this->output;
    }
}
