<?php

namespace Planet17\CLIProcesses\Processes;

/**
 * Class BackgroundProcess.
 *
 * @package Planet17\CLIProcesses\Processes
 */
class BackgroundProcess extends BaseProcess
{
    /** @const POSTFIX_ASYNC */
    private const POSTFIX_ASYNC = ' > /dev/null 2>/dev/null &';

    /** @const POSTFIX_PRINT_PID */
    private const POSTFIX_PRINT_PID = ' echo $!';

    /** @inheritdoc */
    public function getCommand(): string
    {
        return $this->command . self::POSTFIX_ASYNC . self::POSTFIX_PRINT_PID;
    }
}
