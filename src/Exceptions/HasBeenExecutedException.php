<?php

namespace Planet17\CLIProcesses\Exceptions;

use LogicException;
use Throwable;

/**
 * Class HasBeenExecutedException
 */
class HasBeenExecutedException extends LogicException
{
    /** @const DEFAULT_MESSAGE */
    public const DEFAULT_MESSAGE = 'Process must not been executed.';

    /**
     * HasBeenExecutedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
