<?php

namespace Planet17\CLIProcesses\Interfaces;

/**
 * Interface ProcessInterface
 *
 * @package Planet17\MessageQueueProcessManager\Interfaces
 *
 * Classes realize that interface uses like a decorator for provided some settings and additional
 * configuration with provided command.
 *
 * @see ProcessInterface::__construct() - provide command for executing.
 * @see ProcessInterface::getCommand() - show prepated command for executing.
 * @see ProcessInterface::execute() - execute command and keep process_id of executed command process.
 * @see ProcessInterface::getProcessId() - mehtod can return process_id after command executing.
 */
interface ProcessInterface
{
    /**
     * ProcessInterface constructor.
     *
     * @param string $command
     */
    public function __construct(string $command);

    /**
     * Method return compiled string for executing.
     *
     * @return string
     */
    public function getCommand(): string;

    /**
     * Method run executing compiled command.
     *
     * @return ProcessInterface
     */
    public function execute(): ProcessInterface;
}
